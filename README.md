# The BMD Colour Scheme

A TextMate/Sublime Text colour scheme based on the Blackmagic Design logo. Comes in both __Light__ and __Dark__ flavours.

I have only tested it in Sublime Text 3 so far, but the *.tmtheme* files should work with the TextMate editor, and the *.el* should work with emacs.  

## Installation

### Sublime Text

1. Preferences > Browse Packages
2. Open folder in Terminal, and then:
```
mkdir Themes
cd Themes
git clone https://gitlab.com/adriancaruana/the-bmd-colour-scheme
```
3. In Sublime Text: <kbd>CTRL</kbd> <kbd>SHIFT</kbd> <kbd>P</kbd> to open the command palette
4. Select __"UI: Select Color Scheme"__
5. Select __"ThemeBMD - Light"__ or __"ThemeBMD - Dark"__

## Examples:

### ThemeBMD-Light
![ThemeBMD-Light](https://gitlab.com/adriancaruana/the-bmd-colour-scheme/raw/master/examples/light.png)

### ThemeBMD-Dark
![ThemeBMD-Dark](https://gitlab.com/adriancaruana/the-bmd-colour-scheme/raw/master/examples/dark.png)
